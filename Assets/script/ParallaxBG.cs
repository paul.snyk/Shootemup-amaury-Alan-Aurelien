﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBG : MonoBehaviour
{
    public float movespeed;

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3((transform.position.x - movespeed), transform.position.y, transform.position.z);
        if (transform.position.x < 17.70)
            transform.position = new Vector3(0, transform.position.y, transform.position.z);
        
    }
}
