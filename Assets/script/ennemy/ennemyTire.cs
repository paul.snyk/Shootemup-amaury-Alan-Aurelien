﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ennemyTire : MonoBehaviour
{
    public GameObject SpawnPoint;
    public GameObject Missile;
    public float shortTime;
    private float startTime;
    private float elapsedTime;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
        Instantiate(Missile, SpawnPoint.transform.position, Missile.transform.rotation);
       
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime = Time.time - startTime;
        if (elapsedTime >= shortTime)
        {
            
            startTime = Time.time;
            Instantiate(Missile, SpawnPoint.transform.position, Missile.transform.rotation);
            
            
            

        }
    }
}
