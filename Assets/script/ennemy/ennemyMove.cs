﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ennemyMove : MonoBehaviour
{
    public float speed = 5f;
    Transform myTransform;
    public float lifetime;
    // Start is called before the first frame update
    void Start()
    {
        myTransform = GetComponent<Transform>();
        Destroy(gameObject, lifetime);
    }

    // Update is called once per frame
    void Update()
    {
        myTransform.Translate(Vector3.up * Time.deltaTime * speed);
    }
}
