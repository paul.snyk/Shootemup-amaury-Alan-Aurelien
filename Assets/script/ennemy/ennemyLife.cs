﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ennemyLife : MonoBehaviour
{
    public float lifeEnnemy;
    public score _score;
    public Animator animator;

    public bool boss;
    
    // Start is called before the first frame update
    void Start()
    {
        _score = FindObjectOfType<score>();
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (lifeEnnemy <= 0 && !boss)
        {
            StartCoroutine("Dead");
        }
        else if(lifeEnnemy <= 0 && boss)
        {
            _score.scorePlayer += 20;
            Destroy(gameObject);
        }
    }

    IEnumerator Dead()
    {

        animator.SetBool("dead", true);
        yield return new WaitForSeconds(0.1f);
        _score.scorePlayer += 1;
        Destroy(gameObject);
    }


public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Missile"))
        {
            lifeEnnemy = lifeEnnemy - 1;
            Destroy(other.gameObject);
        }
    }
}
