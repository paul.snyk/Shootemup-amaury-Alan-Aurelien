﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class missileMove : MonoBehaviour
{
    public float speed = 12f;
    Transform mytransform;

    // Start is called before the first frame update
    void Start()
    {
        mytransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        mytransform.Translate(Vector3.up * Time.deltaTime * speed);//up (0,1,0):
        Destroy(this.gameObject, 3f);
    }
   
}
