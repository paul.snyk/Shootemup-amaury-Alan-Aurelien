﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    public float speed;
    public Camera MainCamera;
    
    private Vector2 screenBounds;
    private float objectWidth;
    private float objectHeight;
    
    // Start is called before the first frame update
    void Start()
    {
        screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(x: Screen.width, y: Screen.height, MainCamera.transform.position.z));
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x;
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Z)) { transform.Translate(Vector3.up * speed * Time.deltaTime); }
        if (Input.GetKey(KeyCode.Q)) { transform.Translate(Vector3.left * speed * Time.deltaTime); }
        if (Input.GetKey(KeyCode.D)) { transform.Translate(Vector3.right * speed * Time.deltaTime); }
        if (Input.GetKey(KeyCode.S)) { transform.Translate(Vector3.down * speed * Time.deltaTime); }
    }

    private void LateUpdate()
    {
        Vector3 viewPos = transform.position;
        viewPos.x = Mathf.Clamp(value: viewPos.x, min: screenBounds.x * -1 + objectWidth, max: screenBounds.x - objectWidth);
        viewPos.y = Mathf.Clamp(value: viewPos.y, min: screenBounds.y * -1 + objectHeight, max: screenBounds.y - objectHeight);
        transform.position = viewPos;
    }
}
