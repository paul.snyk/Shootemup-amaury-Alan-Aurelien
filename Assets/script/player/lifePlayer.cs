﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class lifePlayer : MonoBehaviour
{
    public float life;
    public Animator animator;

    public TMP_Text textLife;

    public GameObject gameOverPanel;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        life = 10f;
        gameOverPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (life <= 0)
        {

            StartCoroutine("Dead");

        }

        textLife.text = "HP : " + life;
    }

    IEnumerator Dead()
    {
        Debug.Log("gameover");
        animator.SetBool("dead", true);
        yield return new WaitForSeconds(1.8f);
        gameOverPanel.SetActive(true);
        Time.timeScale = 0f;
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("missileEnnemy"))
        {
            life = life - 1;
            Destroy(other.gameObject);
        }
    }
}
