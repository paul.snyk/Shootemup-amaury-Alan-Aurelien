﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playershoot : MonoBehaviour
{
    public GameObject missile;
    public GameObject SpawnPoint;
    public float fireRate;
    private float nextFire;
    
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space) && Time.time > nextFire)
        {
            Debug.Log("tire des missiles");
            nextFire = Time.time + fireRate;
            Instantiate(missile, SpawnPoint.transform.position, Quaternion.identity);
            

        }
    }
   
    

}
