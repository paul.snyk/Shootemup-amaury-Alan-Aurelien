﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollBG : MonoBehaviour
{
    public float speed = 4f;
    
    public GameObject leurre;
    
    private void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * speed * Time.deltaTime);

        if (transform.position.y <= -20)
        {
            Vector3 newPosition = transform.position;
            newPosition.y = leurre.transform.position.y;
            transform.position = newPosition;
        }
    }
}
