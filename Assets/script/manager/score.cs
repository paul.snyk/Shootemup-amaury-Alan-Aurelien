﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class score : MonoBehaviour
{
    public float scorePlayer;

    public TMP_Text textScore;
    public float maxScore;

    public GameObject panelNextLevel;
    public bool boss;
    public GameObject bossPrefab;
    public float limitScore;

    public string nextLevel, currentLevel;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        scorePlayer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (boss && scorePlayer >= limitScore)
        {
            Instantiate(bossPrefab, transform.position,transform.rotation);
            boss = false;
            StopAllCoroutines();
        }
        if (scorePlayer >= maxScore)
        {
            Time.timeScale = 0f;
            panelNextLevel.SetActive(true);
        }
        textScore.text = "Score : " + scorePlayer;
    }

    public void nextLevelMap()
    {
        SceneManager.LoadScene(nextLevel);
    }
    public void retryLevel()
    {
        SceneManager.LoadScene(currentLevel);
    }
}
