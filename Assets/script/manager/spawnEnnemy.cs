﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnEnnemy : MonoBehaviour
{
    public GameObject ennemiPrefab;
    public Vector3 SpawnRange;
    public int NbrEnnemiVague;
    public static bool isInGame = true;
    public float WaveTime;
    public float spawnTime;
    

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(generateWave());

    }
    IEnumerator generateWave()
   
    {

        while (isInGame) //player != null && player.isAlive == true
        {
            for (int i = 0; i < NbrEnnemiVague; i++)
            {
                Vector3 SpawnPos = new Vector3(Random.Range(-SpawnRange.x, SpawnRange.x), Random.Range(SpawnRange.y, SpawnRange.y - 10), 0);
                Instantiate(ennemiPrefab, SpawnPos, ennemiPrefab.transform.rotation);
                yield return new WaitForSeconds(spawnTime);
            }
            yield return new WaitForSeconds(WaveTime);
        }
    }
    
}
