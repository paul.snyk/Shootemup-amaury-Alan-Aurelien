﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPause : MonoBehaviour
{
    public GameObject pause;
    public static bool Activpause = false;
   


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Activpause)
            {
                Reprendre();

            }
            else
            {
                Pause();
            }
        }
    }
    public void Reprendre()
    {
        pause.SetActive(false);
        Time.timeScale = 1f;
        Activpause = false;
    }
    void Pause()
    {
        pause.SetActive(true);
        Time.timeScale = 0f;
        Activpause = true;
    }
    public void LoadMenu()
    {
        Debug.Log("Menu");
        SceneManager.LoadScene("Ecran principal");
    }
    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    public void loadGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("aurelien");
    }
    public void LoadHOF()
    {
        Debug.Log("HOF");
        SceneManager.LoadScene("HOF");
    }
}
